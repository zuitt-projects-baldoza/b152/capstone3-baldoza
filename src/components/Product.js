import {useState} from 'react'
import {Button,Card,Col,Row,Table} from 'react-bootstrap';

//import link component 
import {Link} from 'react-router-dom'
import {Container} from 'react-bootstrap';

export default function ProductsCard({productsProp}){
	
	//console.log(courseProp)
	//console.log("Hello, I will run whenever we update a state with its setter function.")
	const [count,setCount] = useState(0);
	const [seats,setSeats] = useState(30);
	
	function enroll(){
		setCount(count+1);
		//seat++;
		setSeats(seats-1);
		
	}


	return (
		
		<Card className='h-100' bg="dark" style={{ width: '15rem' }}>
		  <Card.Img variant="top" src="https://image.shutterstock.com/image-vector/abstract-grunge-rubber-stamp-text-260nw-151821743.jpg" />
			<Card.Body>     
				<Card.Title>
					{productsProp.name}
				</Card.Title>

			<Card.Text>
				{productsProp.description}
			</Card.Text>
			
			<Card.Text>
				Price: {productsProp.price}
			</Card.Text>
			<Card.Text>
				<Link to={`/products/viewProduct/${productsProp._id}`}className="btn btn-warning">Add to Cart</Link>
			</Card.Text>
			</Card.Body>
		</Card>
		
		)
}

