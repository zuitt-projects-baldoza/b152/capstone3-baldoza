import {useState,useEffect,useContext} from 'react';

import UserContext from '../userContext';

import {Table,Button} from 'react-bootstrap';

import {Navigate} from 'react-router-dom';

export default function AdminDashboard(){

	const {user} = useContext(UserContext);
	console.log(user);

	//state for allCourses
	const [allProducts,setAllProducts]=useState([]);

	//archive function
	function archive(productsId){
		//console.log(courseId);
		fetch(`https://sheltered-mountain-58507.herokuapp.com/products/archive/${productsId}`,{
			method: 'PUT',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			window.location.href="/products";
		})
		
	}

	//activate function
	function activate(productsId){
		//console.log(courseId);
		fetch(`https://sheltered-mountain-58507.herokuapp.com/products/activate/${productsId}`,{
			method: 'PUT',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			window.location.href="/products";
		})
		
	}

	useEffect(()=>{

		if(user.isAdmin){

			//fetch all courses
			fetch('https://sheltered-mountain-58507.herokuapp.com/products',{
				headers: {
					'Authorization': `Bearer ${localStorage.getItem('token')}`
					}
			})
			.then(res => res.json())
			.then(data => {
				//console.log(data);
				setAllProducts(data.map(products=>{
					return (

						<tr key={products._id}>
						<td>{products._id}</td>
						<td>{products.name}</td>
						<td>{products.price}</td>
						<td>{products.isActive ? "Active": "Inactive"}</td>
						<td>
							{
								products.isActive
								?
								<Button variant="danger" className="mx-2" onClick={()=>{archive(products._id)}}>Archive</Button>
								:
								<Button variant="success" className="mx-2" onClick={()=>{activate(products._id)}}>Activate</Button>
							}
						</td>
					</tr>
					)
				}))
			})
		}

	},[])

	return (
		<>
		<h1 className="my-5 text-center">Admin Dashboard</h1>
		<Table striped bordered hover>
			<thead>
				<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Price</th>
				<th>Status</th>
				<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				{/*<tr>
					<td>idSample</td>
					<td>nameSample</td>
					<td>priceSample</td>
					<td>statusSample</td>
					<td>actionsSample</td>
				</tr>*/}
					{allProducts}
			</tbody>
		</Table>
		</>
		)
}