import {Nav,Navbar,Container} from 'react-bootstrap'
import {useContext} from 'react';
import UserContext from '../userContext';
import {Link} from 'react-router-dom';

export default function AppNavBar(){
	const {user} = useContext(UserContext);
	console.log(user);

	return (
		<Navbar bg="warning" expand="lg">
			<Container fluid>
		<Navbar.Brand href="#home">Star Gadgets</Navbar.Brand>
		<Navbar.Toggle aria-controls="basic-navbar-nav" />
		<Navbar.Collapse id="basic-navbar-nav">
		<Nav className="mr-auto">
			<Link to="/" className="nav-link"><span className="navItem">Home</span></Link>
			<Link to="/products" className="nav-link"><span className="navItem">Products</span></Link>
		</Nav>
		<Nav className="ml-auto">
			{
				user.id 
				? 
					user.isAdmin
					?
				<>
					<Link to="/AddProduct" className="nav-link"><span className="navItem">Add Product</span></Link>
					<Link to="/logout" className="nav-link"><span className="navItem">Logout</span></Link>
				</>
				:
					<Link to="logout" className="nav-link"><span className="navItem">Logout</span></Link>
				:
				<>
					<Link to="/register" className="nav-link"><span className="navItem">Register</span></Link>
					<Link to="/login" className="nav-link"><span className="navItem">Login</span></Link>
				</>
			}
		</Nav>
		</Navbar.Collapse>
		</Container>
		</Navbar>
		)
}


