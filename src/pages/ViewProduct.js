import {useState,useEffect,useContext} from 'react';
import {Card,Button,Row,Col} from 'react-bootstrap';
import {useParams,Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../userContext';
import {Navigate} from 'react-router-dom';
import { add, total } from 'cart-localstorage' 


export default function ViewProduct(){

const {productId} = useParams();
//console.log(courseId);

	//console.log(useParams());

const {user} = useContext(UserContext);
console.log(user)

	
const [productDetails, setProductDetails] = useState({

	name: null,
	description: null,
	price:null
})

useEffect(()=>{
	fetch(`https://sheltered-mountain-58507.herokuapp.com/products/getSingleProduct/${productId}`)
	.then(res=> res.json())
	.then(data => {
		//console.log(data);
		setProductDetails({
			name:data.name,
			description:data.description,
			price:data.price
		})
			add({id: productId, name: data.name, price: data.price});
			
	})
},[productId])


	function addToCart(e){
	console.log("Added to Cart!");
	console.log(productId);
	console.log(user.id);



	// const myproduct = {
	// 	id: productId,
	// 	name: {productDetails.name},
	// 	price: {productDetails.price}
	// 	add(myproduct,2)
	// }

// 	localStorage.setItem('price', price);
// 	localStorage.setItem('qty', pr.quantity); 

// var addItem = function (price, qty) {
//     var currentTotal = localStorage.getItem('cartvalue');
//     if(currentTotal)
//         currentTotal = parseFloat(currentTotal) + (parseFloat(price) * parseFloat(qty));
//     else
//         currentTotal = parseFloat(price) * parseFloat(qty);
//     localStorage.setItem('cartvalue', currentTotal);
// };
 //addItem(pr.price, pr.quantity);
	


	//<Navigate to="/ViewCart" replace={true} />


	// 	fetch('http://localhost:4000/orders',{
			
	// 	method: 'POST',
	// 	headers: {
	// 		'Authorization': `Bearer ${token}`,
	// 		"Content-Type": "application/json"},
	// 	body: JSON.stringify({
	// 		userId : user.id,
	// 		//totalAmount: price,
	// 		products: productId
	// 	})
	// 	})
	// .then(res => res.json())
	// .then(data => {
	// 	console.log(data);
	// 	if(data.message === "Order successful."){
	// 		Swal.fire({

	// 			icon:"success",
	// 			title: "Order successful.",
	// 			text: "Thank you for ordering."
	// 		})
	// 	} else {
	// 		Swal.fire({
	// 			icon: "error",
	// 			title: "Order Failed.",
	// 			text: data.message
	// 		})
	// 	}

	// 	})
	}
	return (

		<Row className="mt-5">
			<Col>
				<Card bg="dark">
				<Card.Body className="text-center">
					<Card.Title>{productDetails.name}</Card.Title>
					<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text>{productDetails.description}</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>{productDetails.price}</Card.Text>
						</Card.Body>
							{
							user.id && user.isAdmin === false
							? <Button variant="warning" className="btn-block" onClick={addToCart}>Add to Cart</Button>

							: <Link className="btn btn-danger btn-block" to="/login">Login To Enroll</Link>
						}

					</Card>
				</Col>
			</Row>
		)
}

