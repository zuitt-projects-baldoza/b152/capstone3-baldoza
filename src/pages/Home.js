import Banner from '../components/Banner';
//import Highlights from '../components/Highlights';

//import {Container} from 'react-bootstrap'


export default function Home(){

	let bannerData = {
		title: "Star Gadgets ☆",
		description: "Pre-loved gadgets at an affordable price!",
		buttonText:"View Our Products",
		destination: "/products"
	}

	return (
		<>
		<Banner bannerProp={bannerData}/>

    </>
  );
}

