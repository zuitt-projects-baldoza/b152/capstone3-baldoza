import {useState,useContext,useEffect} from 'react';
import {Form,Button} from 'react-bootstrap';

import Swal from "sweetalert2";

import UserContext from '../userContext';

import {Navigate} from 'react-router-dom';

export default function AddProduct(){
	const {user,setUser} = useContext(UserContext);

	const [name,setName]=useState("");
	const [description,setDescription]=useState("");
	const [price,setPrice]=useState("");


	const [isActive,setIsActive] = useState(false);

	useEffect(()=>{

		if(name !== "" && description !== "" && price !== ""){
			setIsActive(true)
		} else {
			setIsActive(false)
		}

	},[name,description,price])

	function createProduct(e){

	e.preventDefault();

let token = localStorage.getItem('token');
let isAdmin = localStorage.getItem('isAdmin');

fetch('https://sheltered-mountain-58507.herokuapp.com/products/',{

		method: 'POST',
		headers: {
			'Authorization': `Bearer ${token}`,
			"Content-Type": "application/json"},
		body: JSON.stringify({
			name: name,
			description: description,
			price: price
		})
		})
	.then(res => res.json())
	.then(data => {
		console.log(data);
		console.log(isAdmin);

		if(data._id){
			Swal.fire({
				icon: "success",
				title: "Product Creation Successful"
			})

			window.location.href = "/login";

		} else {
			Swal.fire({

				icon: "error",
				title: "Product Creation Failed.",
				text: data.message
			})

		}

			})
		}

	return (
			<>
			<h1 className="my-5 text-center">Create a Product</h1>
			<Form onSubmit={e => createProduct(e)}>
			<Form.Group>
					<Form.Label>
					Product name:
					</Form.Label>
					<Form.Control type="text" placeholder="Enter Product Name" required value={name} onChange={e => {setName(e.target.value)}}/>
			</Form.Group>	

			<Form.Group>
					<Form.Label>
					Description:
					</Form.Label>
					<Form.Control type="text" placeholder="Enter Description" required value={description} onChange={e => {setDescription(e.target.value)}}/>
			</Form.Group>	

			<Form.Group>
					<Form.Label>
					Price:
					</Form.Label>
					<Form.Control type="number" placeholder="Enter Price" required value={price} onChange={e => {setPrice(e.target.value)}}/>
			</Form.Group>
		
					{
					isActive

					? <Button variant="warning" type="submit">Submit</Button>
					: <Button variant="warning" disabled>Submit</Button>
				}

			</Form>
		</>

		)
}


