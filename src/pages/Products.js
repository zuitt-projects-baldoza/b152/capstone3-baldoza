import {useState,useEffect,useContext} from 'react'
import Banner from '../components/Banner';
import ProductsCard from '../components/Product';
import AdminDashboard from '../components/AdminDashboard';
import {Button,Card,Col,Row,Table} from 'react-bootstrap';


import UserContext from '../userContext'

export default function Products(){

	const {user} = useContext(UserContext);
	console.log(user);
	console.log(user.firstName);
	//console.log(coursesData)

	let productsBanner = {

		title: "Latest Gadgets",
		description: "The list is always updated by the admin.",
		buttonText: "Register/Login to Order",
		destination: "/register"
	}

	let loggedInBanner = {

		title: `Hello, ${user.firstName} ${user.lastName}! `,
		description: "Here are the list of our items.",
		buttonText: "View Cart",
		destination: "/ViewCart"
	}

const [productsArray,setProductsArray] = useState([])

useEffect(()=>{

	fetch("https://sheltered-mountain-58507.herokuapp.com/products/active")
	.then(res=>res.json())
	.then(data=>{
		//console.log(data)

		setProductsArray(data.map(products=>{
			return(
				<ProductsCard key={products._id} productsProp={products} />
				)
		}))
	})
},[])

// 	let coursesComponents = coursesData.map(course=>{
// 		return <CourseCard courseProp={course} key={course.id}/>
// 	})

// 	console.log(coursesComponents)

	return (
		user.id 
				? 
					user.isAdmin
					?
					<AdminDashboard />
					:
						<>
		<Banner bannerProp={loggedInBanner}/>
		{productsArray}
			
		</>
		:
		<>
		<Banner bannerProp={productsBanner}/>
		<Row>{productsArray}</Row>

		</>
		)
}

