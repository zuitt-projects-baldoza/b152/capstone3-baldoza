import {useState,useEffect} from 'react';

import {Container} from 'react-bootstrap';

//import react-router-dom components to simulate/implement page routing in our app.
import {BrowserRouter as Router} from 'react-router-dom';
import {Route,Routes} from 'react-router-dom';

// import Banner from './components/Banner'
import AppNavBar from './components/AppNavBar';
import Home from './pages/Home';
import './App.css';

import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Products from './pages/Products';
import ViewProduct from './pages/ViewProduct';
import ViewCart from './pages/ViewCart';
import AddProduct from './pages/AddProduct';

import { UserProvider } from './userContext';

export default function App(){

const [user,setUser] = useState({

  firstName: null,
  lastName: null,
  id:null,
  isAdmin:null

})

useEffect(()=>{

  //fetch() - to get userDetails whenever page refresh and main component initially renders.
  fetch('https://sheltered-mountain-58507.herokuapp.com/users/getUserDetails',{

    method:'GET',
    headers: {

      'Authorization': `Bearer ${localStorage.getItem('token')}`
    }

  })
  .then(res => res.json())
  .then(data => {
    //console.log(data);
    setUser({

      firstName: data.firstName,
      lastName: data.lastName,
      id: data._id,
      isAdmin: data.isAdmin
    

    })
  })

},[])

//console.log(user);

  const unsetUser = () => {

    localStorage.clear()

  }

  return (
    <>
      <UserProvider value={{user,setUser,unsetUser}} >
      <Router>
        <AppNavBar />
        <Container>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/login" element={<Login />} />
            <Route path="/register/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/products" element={<Products />} />
            <Route path="/products/viewProduct/:productId" element={<ViewProduct />} /> 
             <Route path="/addProduct" element={<AddProduct />} />
             <Route path="/ViewCart" element={<ViewCart />} />
          </Routes>
        </Container>
      </Router>
      </UserProvider>
    </>
  )
}